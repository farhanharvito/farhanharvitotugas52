import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {persistor, store} from './src/stateManagement/utils/store';
import {SafeAreaView} from 'react-native-safe-area-context';
import {StyleSheet, Text, View, StatusBar} from 'react-native';
import React from 'react';
import Routing from './src/stateManagement/Routing';

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <SafeAreaView style={{flex: 1}}>
          <StatusBar barStyle={'dark-content'} />
          <Routing />
        </SafeAreaView>
      </PersistGate>
    </Provider>
  );
};

export default App;
