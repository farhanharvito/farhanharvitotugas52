import {Text, View} from 'react-native';
import React from 'react';

const initialState = {
  product: [],
};

const dataReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_DATA':
      return {
        ...state,
        product: action.data,
      };

    case 'UPDATE_DATA':
      //ambil data product sebelumnya
      var newData = [state.product];

      //karena data yang ada adalah array, maka cari index data yang akan dirubah melalui key
      //keynya adalah id dari data yang ingin dirubah
      //idnya berada didalam object action.data yang dikirim dari fungsi updateData() sebelumnya
      var findIndex = state.product.findIndex(value => {
        return value.id === action.data.id;
      });

      //ganti data berdasarkan indexnya dg data yg baru
      newData[findIndex] = action.data;
      return {
        ...state,
        product: newData,
      };

    case 'DELETE_DATA':
      //ambil data product sebelumnya
      var newData = [state.product];

      //karena data yang ada adalah array, maka cari index data yang akan dirubah melalui key
      //keynya adalah id dari data yang ingin dirubah
      //idnya berada didalam object action.data yang dikirim dari fungsi updateData() sebelumnya
      var findIndex = state.product.findIndex(value => {
        return value.id === action.id;
      });

      //hapus data bdsk indexnya dg fungsi splice yang ada pada pengolahan data tipe array
      newData.splice(findIndex, 1);
      return {
        ...state,
        product: newData,
      };

    default:
      return state;
  }
};

export default dataReducer;
